define([
  'views/generic',
  'text!templates/create/toolbar.html',
  'text!templates/create/content.html'
], function(GenericView, ToolbarTemplate, ContentTemplate) {

	var View = GenericView.extend({
		
		id: "gauge-create",

		name: "GaugeCreateView",

		// Cache the template functions
		templates: {
			".toolbar": _.template(ToolbarTemplate),
			".content": _.template(ContentTemplate)
		},

		// The DOM events specific to an item.
		events: {
			// taps
			"tap .btn_back"			: "back",
			"tap .gaugefy_select"	: "select",
			"tap .btn_clear"		: "clear",
			"tap .btn_measurement"	: "byMeasurement",
			"tap .btn_stitches"		: "byStitches",
			"tap .btn_repeat"		: "byRepeat",
			"tap .btn_show_ins"		: "showInstructions",
			"tap .floaty .close"	: "hideInstructions",

			// clicks
			"click .btn_back"		: "back",
			"click .gaugefy_select"	: "select",
			"click .btn_clear"		: "clear",
			"click .btn_measurement": "byMeasurement",
			"click .btn_stitches"	: "byStitches",
			"click .btn_repeat"		: "byRepeat",
			"click .btn_show_ins"	: "showInstructions",
			"click .floaty .close"	: "hideInstructions",

			// keyups
			"keyup .goal_width"		: "changed",
			"keyup .goal_length"	: "changed",
			"keyup .goal_stitches"	: "changed",
			"keyup .goal_rows"		: "changed",
			"keyup .goal_repeat_x"	: "changed",
			"keyup .goal_repeat_y"	: "changed"
		},

		initialize: function() {
			// call super
			GenericView.prototype.initialize.call(this);

			_.bindAll(this, 'render', "refreshView", "setModel", "clear", "back", "showInstructions", "hideInstructions", "changed");
		},

		render: function() {
			var data = this.model.toJSON();
			data.cid = this.model.cid;
			// call super
			GenericView.prototype.render.call(this, data);
			return this;
		},

		changed: function(e) {
			switch(e.target.name) {
				case "goal_width":
				case "goal_length":
					var goal_width = this.$('.goal_width').val();
					var goal_length = this.$('.goal_length').val();

					if(goal_width != '' && goal_length != '') {
						var original = {
							"goal_width"	: parseFloat(this.model.get("goal_width")),
							"goal_length"	: parseFloat(this.model.get("goal_length"))
						};
						var current = {
							"goal_width"	: parseFloat(goal_width),
							"goal_length"	: parseFloat(goal_length)
						};
						console.log(JSON.stringify(original), JSON.stringify(current), _.isEqual(original, current));
						var button = this.$(".btn_measurement");
						if(_.isEqual(original, current)) {
							button.removeClass("show");
						} else {
							button.addClass("show");
						}
					}
					break;
				case "goal_stitches":
				case "goal_rows":
					var goal_stitches = this.$('.goal_stitches').val();
					var goal_rows = this.$('.goal_rows').val();

					if(goal_stitches != '' && goal_rows != '') {
						var original = {
							"goal_stitches"	: parseFloat(this.model.get("goal_stitches")),
							"goal_rows"	: parseFloat(this.model.get("goal_rows"))
						};
						var current = {
							"goal_stitches"	: parseFloat(goal_stitches),
							"goal_rows"	: parseFloat(goal_rows)
						};
						console.log(JSON.stringify(original), JSON.stringify(current), _.isEqual(original, current));
						var button = this.$(".btn_stitches");
						if(_.isEqual(original, current)) {
							button.removeClass("show");
						} else {
							button.addClass("show");
						}
					}
					break;
				case "goal_repeat_x":
				case "goal_repeat_y":
					var goal_repeat_x = this.$('.goal_repeat_x').val();
					var goal_repeat_y = this.$('.goal_repeat_y').val();

					if(goal_repeat_x != '' && goal_repeat_y != '') {
						var original = {
							"goal_repeat_x"	: parseFloat(this.model.get("goal_repeat_x")),
							"goal_repeat_y"	: parseFloat(this.model.get("goal_repeat_y"))
						};
						var current = {
							"goal_repeat_x"	: parseFloat(goal_repeat_x),
							"goal_repeat_y"	: parseFloat(goal_repeat_y)
						};
						console.log(JSON.stringify(original), JSON.stringify(current), _.isEqual(original, current));
						var button = this.$(".btn_repeat");
						if(_.isEqual(original, current)) {
							button.removeClass("show");
						} else {
							button.addClass("show");
						}
					}
					break;
			}
		},

		refreshView: function() {
			this.$('.goal_width').val(this.model.escape("goal_width"));
			this.$('.goal_length').val(this.model.escape("goal_length"));
			this.$('.goal_stitches').val(this.model.escape("goal_stitches"));
			this.$('.goal_rows').val(this.model.escape("goal_rows"));
			this.$('.goal_repeat_x').val(this.model.escape("goal_repeat_x"));
			this.$('.goal_repeat_y').val(this.model.escape("goal_repeat_y"));
		},

		select: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
			
				var target = e.target;

				// remove selected state from all items
				this.$('.gaugefy_selected').removeClass('gaugefy_selected');

				// disable all inputs
				this.$('input').attr('disabled', 'disabled');
				
				// show selected state for target				
				$(target).parents('ul').addClass("gaugefy_selected");

				// enable selected inputs
				this.$(".gaugefy_selected").find("input").removeAttr("disabled");

			}
		},

		showInstructions: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.$(".floaty").css({top:10});
			}
		},

		hideInstructions: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.$(".floaty").css({top:this.$(".floaty").height() * -1 - 10});
			}
		},

		byMeasurement: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				var goal_width = this.$('.goal_width').val();
				var goal_length = this.$('.goal_length').val();

				if(goal_width == '' || goal_length == ''){ 
					navigator.notification.alert("Please enter the 2 values.", null, "Oops", "OK");
					//alert("Please enter the 2 values.");
				} else {
					this.model.createByMeasurement(goal_width, goal_length);
					this.$('.gaugefy_go').removeClass("show");
				}
			}
		},

		byStitches: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				var goal_stitches = this.$('.goal_stitches').val();
				var goal_rows = this.$('.goal_rows').val();

				if(goal_stitches == '' && goal_rows == ''){ 
					navigator.notification.alert("Please enter the 2 values.", null, "Oops", "OK");
					//alert("Please enter the 2 values.");
				} else {
					this.model.createByStitches(goal_stitches, goal_rows);
					this.$('.gaugefy_go').removeClass("show");
				}
			}
		},

		byRepeat: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				var goal_repeat_x = this.$('.goal_repeat_x').val();
				var goal_repeat_y = this.$('.goal_repeat_y').val();

				if(goal_repeat_x == '' && goal_repeat_y == ''){ 
					navigator.notification.alert("Please enter the 2 values.", null, "Oops", "OK");
					//alert("Please enter the 2 values.");
				} else {
					this.model.createByRepeat(goal_repeat_x, goal_repeat_y);
					this.$('.gaugefy_go').removeClass("show");
				}
			}
		},

		clear: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.$(".goal_stitches, .goal_rows, .goal_width, .goal_length, .goal_repeat_x, .goal_repeat_y").val("");
			}
		},

		back: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.unsetModel();
				window.location.href = $(e.target).attr("href");
			}
		},

		setModel: function(model) {
			// call super
			GenericView.prototype.setModel.call(this, model);

			this.model.bind('change', this.refreshView);
		}

	});

	return View;

}); // define