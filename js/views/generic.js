define([
  'text!templates/base.html'
], function(BaseTemplate){

	var genericView = Backbone.View.extend({
		name: "GenericGaugefyView",
		base: _.template(BaseTemplate),
		templates: {},
		taps: {},
		tagName:  "div",
		log: function() {
			var prefix = "Views > " + this.name + " >";
			var items = Array.prototype.slice.call(arguments, 0);
			items.unshift(prefix);
			console.log.apply(console, items);
		},
		initialize: function() {
			_.bindAll(this, 'scrollRefresh', 'preventScroll', 'delegateTaps');
			
			// render base template and append to stage
			$(this.el).html(this.base()).appendTo("#jqt");

			// bind tap handlers
			this.delegateTaps();

			// refresh scroll
			this.scrollRefresh();
			$(this.el).bind('scrollRefresh', this.scrollRefresh);
		},
		delegateTaps: function() {
			_.each(this.taps, function(handler, selector) {
				// console.log(handler+" "+selector);
				// console.log(this[handler]);
          		$(this.el).delegate(selector, 'tap', this[handler]);
			}, this);
		},
		setModel: function(model) {
			this.model = model;

			// rerender with the new model data
			this.render();

			// scroll to top
			this.$(".content")[0].scrollTop = 0;
		},
		unsetModel: function() {
			// unbind to the model's change event
			this.model.unbind('change');
		},
		render: function(data) {
			_.each(this.templates, function(template, element) {
				if(typeof data != "undefined") {
					this.$(element).html(template(data));
				} else {
					this.$(element).html(template(this.model.toJSON()));
				}
			}, this);

			// refresh scroll
			this.scrollRefresh();

			return this;
		},
		scrollRefresh: function() {
			if(this.$(".content").height() > this.$(".scroller").height()){
				this.$(".scroller").unbind('touchmove');
			} else {
				this.$(".scroller").bind('touchmove', this.preventScroll);
			}
		},
		preventScroll: function(e) {
			e.preventDefault();
		},

		// tap and click event handlers
		cancelClick: function(e) {
			if("ontouchstart" in window) {
				e.preventDefault();
			}
		},
		processTap: function(e) {
			window.location.href = $(e.target).attr("href");
		},
		disableTap: function(e) {
			e.preventDefault();
		}
	});

	return genericView;

});