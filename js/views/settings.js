define([
  'views/generic',
  'text!templates/settings/toolbar.html',
  'text!templates/settings/content.html'
], function(GenericView, ToolbarTemplate, ContentTemplate) {

	var Settings = GenericView.extend({
		
		id: "about",

		name: "Settings",

		// Cache the template functions
		templates: {
			".toolbar": _.template(ToolbarTemplate),
			".content": _.template(ContentTemplate)
		},

		events: {
			"tap .units .imperial"		: "setUnitImperial",
			"tap .units .metric"		: "setUnitMetric",
			"tap .needles .imperial"	: "setNeedleImperial",
			"tap .needles .metric"		: "setNeedleMetric",
			"tap .btn_back"				: "processTap",
			"click .units .imperial" 	: "setUnitImperial",
			"click .units .metric" 		: "setUnitMetric",
			"click .needles .imperial"	: "setNeedleImperial",
			"click .needles .metric" 	: "setNeedleMetric",
			"click .btn_back"			: "cancelClick"
		},

		// taps: {
		// 	inch: function(e) {
		// 		$(document).trigger("setGlobals", {unit:"inch"});
		// 	},
		// 	cm: function(e) {
		// 		$(document).trigger("setGlobals", {unit:"cm"});
		// 	},
		// 	needle_inch: function(e) {
		// 		$(document).trigger("setGlobals", {needle:"inch"});
		// 	},
		// 	needle_cm: function(e) {
		// 		$(document).trigger("setGlobals", {needle:"cm"});
		// 	},
		// 	btn_back: function(e) {
		// 		window.location.href = $(e.target).attr("href");
		// 	}
		// },

		initialize: function() {
			// call super
			GenericView.prototype.initialize.call(this);

			_.bindAll(this, 'render', "setUnitImperial", "setUnitMetric", "setNeedleImperial", "setNeedleMetric", "refreshGlobals");

			// bind to app wide unit change
			$(document).bind("refreshGlobals", this.refreshGlobals);

			// store units setting for reference
			this.globals = this.options;

			this.render(this.globals);
		},

		setUnitImperial: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				$(document).trigger("setGlobals", {unit:"imperial"});
			}
		},

		setUnitMetric: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				$(document).trigger("setGlobals", {unit:"metric"});
			}
		},

		setNeedleImperial: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				$(document).trigger("setGlobals", {needle_unit:"imperial"});
			}
		},

		setNeedleMetric: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				$(document).trigger("setGlobals", {needle_unit:"metric"});
			}
		},

		refreshGlobals: function(e, globals) {
			this.globals = globals;
			// unit
			this.$(".units a").removeClass("selected");
			this.$(".units ."+globals.unit).addClass("selected");
			// needle
			this.$(".needles a").removeClass("selected");
			this.$(".needles ."+globals.needle_unit).addClass("selected");
		},

		goBack: function(e) {
			if(!("ontouchstart" in window) || e.type == "tap") {
				window.location.href = $(this).attr("href");
			}
		}

	});

	return Settings;
	
}); // define