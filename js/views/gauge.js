define([
  'views/generic',
  'text!templates/gauge/toolbar.html',
  'text!templates/gauge/content.html',
  'text!templates/gauge/edit.html',
  'text!templates/gauge/view.html'
], function(GenericView, ToolbarTemplate, ContentTemplate, EditSubTemplate, ViewSubTemplate) {

	var View = GenericView.extend({
		
		id: "gauge",

		name: "GaugeView",

		// Cache the template functions
		templates: {
			".toolbar": _.template(ToolbarTemplate),
			".content": _.template(ContentTemplate),
			".editContent": _.template(EditSubTemplate),
			".viewContent": _.template(ViewSubTemplate)
		},

		// The DOM events specific to an item.
		events: {
			// taps
			"tap .btn_edit"				: "toggleEditMode",
			"tap .btn_cancel"			: "cancelEditMode",
			"tap .btn_back"				: "cancel",
			"tap .btn_clear"			: "clear",
			"tap .btn_save"				: "save",
			"tap .repeat_help_icon"		: "showHelp",
			"tap .floaty .close"		: "hideHelp",
			"tap .adjustgo_btn"			: "processTap",
			"tap .creatego_btn"			: "processTap",

			// clicks
			"click .btn_edit"			: "toggleEditMode",
			"click .btn_cancel"			: "cancelEditMode",
			"click .btn_back"			: "cancel",
			"click .btn_clear"			: "clear",
			"click .btn_save"			: "save",
			"click .repeat_help_icon"	: "showHelp",
			"click .floaty .close"		: "hideHelp",
			"click .adjustgo_btn"		: "cancelClick",
			"click .creatego_btn"		: "cancelClick"
		},

		animEnd: function(e) {
			console.log("woo!");
		},

		initialize: function(globals) {
			// call super
			GenericView.prototype.initialize.call(this);

			_.bindAll(this, 'render', "save", "clear", "cancel", "back", "showHelp", "hideHelp", "refreshGlobals", "modelChanged");

			// bind to app wide unit change
			$(document).bind("refreshGlobals", this.refreshGlobals);

			// store units setting for reference
			this.globals = globals;
		},

		render: function() {
			var data = this.model.toJSON();
			data.unit = (this.globals.unit == "imperial") ? "inch" : "cm";
			data.needle_unit = (this.globals.needle_unit == "imperial") ? "US" : "mm";
			data.cid = this.model.cid;
			// call super
			GenericView.prototype.render.call(this, data);
			if(this.model.isNew()) {
				$(this.el).addClass("new");
			} else {
				$(this.el).removeClass("new edit");
			}
			return this;
		},

		save: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				var swatch_stitches = this.$('.stitches').val();
				var swatch_rows = this.$('.rows').val();
				var swatch_width = this.$('.width').val();
				var swatch_length = this.$('.length').val();
			 
				if(swatch_stitches == '' || swatch_rows == '' || swatch_width == '' || swatch_length == ''){ 
				 	navigator.notification.alert("Please fill out all fields.", null, "Oops", "OK");
				 	e.preventDefault();
				 	//alert("Please fill out all fields.", null, "Oops", "OK");
				} else {
					this.$('.gauge_show').addClass("show");
					
					this.model.setGaugeValues({
						"title"				: this.$(".title").val(),
						"yarn"		 		: this.$(".yarn").val(),
						"needle"			: this.$(".needle").val(),
						"swatch_stitches"	: swatch_stitches,
						"swatch_rows"		: swatch_rows,
						"swatch_width"		: swatch_width,
						"swatch_length"		: swatch_length
					});

					this.model.save();

					if(!this.model.isNew()) {
						$(this.el).removeClass("edit");
					}
				}
				window.location.href = "#/gauges/"+this.model.cid;
			}
		},

		clear: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.$(".editContent").find(".title, .stitches, .rows, .width, .length, .yarn, .needle").val("");
			}
		},

		back: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.unsetModel();
				this.el.unbind();
			}
		},

		showHelp: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.$(".floaty").css({top:10});
			}
		},

		hideHelp: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.$(".floaty").css({top:this.$(".floaty").height() * -1 - 10});
			}
		},

		refreshGlobals: function(e, globals) {
			this.globals = globals;
		},

		toggleEditMode: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				$(this.el).addClass("edit");
				this.scrollRefresh();
			}
		},
		
		cancelEditMode: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				$(this.el).removeClass("edit");
				var data = this.model.toJSON();
				data.cid = this.model.cid;
				this.$(".editContent").html(this.templates[".editContent"](data));
				this.scrollRefresh();
			}
		},

		modelChanged: function() {
			var data = this.model.toJSON();
			data.unit = (this.globals.unit === "imperial") ? "inch" : "cm";
			data.needle_unit = (this.globals.needle_unit === "imperial") ? "US" : "mm";
			data.cid = this.model.cid;
			this.$(".toolbar").html(this.templates[".toolbar"](data));
			this.$(".viewContent").html(this.templates[".viewContent"](data));
		},

		setModel: function(model) {
			// call super
			GenericView.prototype.setModel.call(this, model);

			this.model.bind('change', this.modelChanged);
		},

		cancel: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				if(this.model.isNew()) {
					this.model.destroy();
				}
				window.location.href = "#/";
			}
		}
		
	});

	return View;

}); // define