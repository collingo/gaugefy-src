define([
  'views/generic',
  'text!templates/adjust/toolbar.html',
  'text!templates/adjust/content.html'
], function(GenericView, ToolbarTemplate, ContentTemplate) {

	var View = GenericView.extend({
		
		id: "gauge-adjust",

		name: "GaugeAdjustView",

		// Cache the template functions
		templates: {
			".toolbar": _.template(ToolbarTemplate),
			".content": _.template(ContentTemplate)
		},

		// The DOM events specific to an item.
		events: {
			// taps
			"tap .btn_adjust"		: "calculate",
			"tap .btn_save"			: "save",
			"tap .btn_clear"		: "clear",
			"tap .btn_show_ins"		: "showInstructions",
			"tap .floaty .close"	: "hideInstructions",
			"tap .btn_back"			: "back",

			// clicks
			"click .btn_adjust"		: "calculate",
			"click .btn_save"		: "save",
			"click .btn_clear"		: "clear",
			"click .btn_show_ins"	: "showInstructions",
			"click .floaty .close"	: "hideInstructions",
			"click .btn_back"		: "back",

			// keyups
			"keyup .pattern_gauge_stitches" : "changed",
			"keyup .pattern_gauge_rows" 	: "changed",
			"keyup .pattern_final_stitches" : "changed",
			"keyup .pattern_final_rows" 	: "changed"
		},

		initialize: function() {
			// call super
			GenericView.prototype.initialize.call(this);

			_.bindAll(this, 'render', "save", "clear", "back", "calculate", "changed");
		},

		render: function() {
			var data = this.model.toJSON();
			data.cid = this.model.cid;
			data.changed = false;
			if(this.model.hasChanged("pattern_gauge_stitches")
				|| this.model.hasChanged("pattern_gauge_rows")
				|| this.model.hasChanged("pattern_final_stitches")
				|| this.model.hasChanged("pattern_final_rows")) {
				data.changed = true;
			}
			// call super
			GenericView.prototype.render.call(this, data);
			return this;
		},

		showInstructions: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.$(".floaty").css({top:10});
			}
		},

		hideInstructions: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.$(".floaty").css({top:this.$(".floaty").height() * -1 - 10});
			}
		},

		changed: function(e) {
			var pattern_gauge_stitches = this.$('.pattern_gauge_stitches').val();
			var pattern_gauge_rows = this.$('.pattern_gauge_rows').val();	
			var pattern_final_stitches = this.$('.pattern_final_stitches').val();	
			var pattern_final_rows = this.$('.pattern_final_rows').val();

			if(pattern_gauge_stitches != '' && pattern_gauge_rows != '' && pattern_final_stitches != '' && pattern_final_rows != '') {
				var original = {
					"pattern_gauge_stitches"	: this.model.get("pattern_gauge_stitches"),
					"pattern_gauge_rows"		: this.model.get("pattern_gauge_rows"),
					"pattern_final_stitches"	: this.model.get("pattern_final_stitches"),
					"pattern_final_rows"		: this.model.get("pattern_final_rows")
				};
				var current = {
					"pattern_gauge_stitches"	: pattern_gauge_stitches,
					"pattern_gauge_rows"		: pattern_gauge_rows,
					"pattern_final_stitches"	: pattern_final_stitches,
					"pattern_final_rows"		: pattern_final_rows
				};
				if(_.isEqual(original, current)) {
					this.$(".btn_adjust").removeClass("show");
					this.$(".adjust_show").addClass("show");
				} else {
					this.$(".btn_adjust").addClass("show");
					this.$(".adjust_show").removeClass("show");
				}
			}
		},

		calculate: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				var pattern_gauge_stitches = this.$('.pattern_gauge_stitches').val();
				var pattern_gauge_rows = this.$('.pattern_gauge_rows').val();	
				var pattern_final_stitches = this.$('.pattern_final_stitches').val();	
				var pattern_final_rows = this.$('.pattern_final_rows').val();	
	            
				if(pattern_gauge_stitches == '' || pattern_gauge_rows == '' || pattern_final_stitches == '' || pattern_final_rows == ''){ 
				 	navigator.notification.alert("Please fill out all fields.", null, "Oops", "OK");
				 	//alert("Please fill out all fields.", null, "Oops", "OK");
				} else {
					this.model.adjustCalculate(pattern_gauge_stitches, pattern_gauge_rows, pattern_final_stitches, pattern_final_rows);

					this.$('.adjust_show').addClass("show");

					this.scrollRefresh();
				}
			}
		},

		save: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.model.set({
					pattern_gauge_stitches	: this.$(".pattern_gauge_stitches").val(),
					pattern_gauge_rows		: this.$(".pattern_gauge_rows").val(),
					pattern_final_stitches	: this.$(".pattern_final_stitches").val(),
					pattern_final_rows		: this.$(".pattern_final_rows").val()
				});

				this.model.save();
			}
		},

		clear: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.$(".pattern_gauge_stitches, .pattern_gauge_rows, .pattern_final_stitches, .pattern_final_rows").val("");
				this.$('.adjust_show').removeClass("show");
			}
		},

		back: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				this.unsetModel();
				window.location.href = $(e.target).attr("href");
			}
		},

		setModel: function(model) {
			// call super
			GenericView.prototype.setModel.call(this, model);

			this.model.bind('change', this.render);
		}
		
	});

	return View;

}); // define