define([
  'collections/gauges',
  'views/generic',
  'text!templates/home/toolbar.html',
  'text!templates/home/content.html',
  'views/home/li'
], function(GaugeCollection, GenericView, ToolbarTemplate, ContentTemplate, LiView) {

	var HomeView = GenericView.extend({
		
		id: "home",

		className: "current",

		name: "GaugeList",

		editmode: false,

		// Cache the template functions
		templates: {
			".toolbar": _.template(ToolbarTemplate),
			".content": _.template(ContentTemplate)
		},

		events: {
			"tap .new"				: "processTap",
			"tap .settingButton"	: "processTap",
			"tap .btn_editMode"		: "toggleEditMode",
			"click .new"			: "cancelClick",
			"click .settingButton"	: "cancelClick",
			"click .btn_editMode"	: "toggleEditMode",
		},

		initialize: function() {
			// call super
			GenericView.prototype.initialize.call(this);

			_.bindAll(this, 'render', "appendGauge", "removeGauge", "processTap", "addGaugeListItem", "removeGaugeListItem", "toggleEditMode");

			this.render();

			this.collection.bind('add', this.addGaugeListItem);
			this.collection.bind('remove', this.removeGaugeListItem);
		},

		render: function() {
			// call super
			GenericView.prototype.render.call(this, {});

			this.$(".gaugelist").empty();
			this.collection.each(this.appendGauge);
			return this;
		},

		appendGauge: function( gauge ) {
			var li = new LiView({model: gauge});
			this.$(".gaugelist").prepend(li.render().el);
			this.scrollRefresh();
		},

		removeGauge: function( gauge ) {
			var id = gauge.get("id");
			var selector = "." + id;
			this.$(selector).parent().remove();
			this.scrollRefresh();
		},


		// event handlers
		addGaugeListItem: function( gauge ) {
			this.appendGauge(gauge);
		},
		removeGaugeListItem: function( gauge ) {
			this.removeGauge(gauge);
		},
		processTap: function(e) {
			if(!this.editmode) {
				window.location.href = $(e.target).attr("href");
			}
		},
		toggleEditMode: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				$(this.el).toggleClass("edit");
				if($(this.el).hasClass("edit")) {
					this.$(".btn_editMode").text("Done");
					this.editmode = true;
				} else {
					this.$(".btn_editMode").text("Edit");
					this.editmode = false;
				}
				this.scrollRefresh();
			}
		}
	});

	return HomeView;
});