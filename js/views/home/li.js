define([
  'views/generic',
  'text!templates/home/li.html'
], function(GenericView, Template) {
	
	var View = GenericView.extend({
		
		tagName:  "li",

		name: "GaugeListItem",

		// Cache the template function for a single item.
		template: _.template(Template),

		// The DOM events specific to an item.
		events: {
			"tap .title" : "showGauge",
			"tap .delete": "delete",
			"click .title" : "showGauge",
			"click .delete": "delete"
		},

		initialize: function() {
			_.bindAll(this, 'render', 'showGauge', 'delete', 'destroyed');

			// bind to the model's change event to rerender the view
			this.model.bind('change', this.render);
			this.model.bind('destroy', this.destroyed);
		},

		render: function() {
			$(this.el).html(this.template(this.model.toJSON()));
			return this;
		},

		showGauge: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				if(!($(this.el).parents("#home").hasClass("edit"))) {
					window.location.href = "#/gauges/"+this.model.cid;
				}
			}
		},

		delete: function(e) {
			e.preventDefault();
			if(!("ontouchstart" in window) || e.type == "tap") {
				if($(this.el).parents("#home").hasClass("edit")) {
					this.model.destroy();
				}
			}
		},

		destroyed: function(e) {
			$(this.el).remove();
		}

	});

	return View;

}); // define