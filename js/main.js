// Require.js allows us to configure shortcut alias
// There usage will become more apparent futher along in the tutorial.
require.config({
  paths: {
    // loader: 'libs/backbone/loader',
    // Zepto: 'libs/zepto/zepto',
    // Underscore: 'libs/underscore/underscore',
    // Backbone: 'libs/backbone/backbone',
    templates: '../templates'
  }
});

require([
	// Load our app module and pass it to our definition function
	'app',
], function(app) {
	window.app = app;
});