define([], function(){

	var GaugefyModel = Backbone.Model.extend({
		name: "GenericGaugefyModel",
		log: function() {
			var prefix = "Models > " + this.name + " >";
			var items = Array.prototype.slice.call(arguments, 0);
			items.unshift(prefix);
			console.log.apply(console, items);
		}
	});
	var Gauge = GaugefyModel.extend({

		defaults: {
			
			// generic
			title: "",
			yarn: "",
			needle: "",

			// swatch
			swatch_stitches: 0,
			swatch_rows: 0,
			swatch_width: 0,
			swatch_length: 0,

			// gauge
			gauge_stitches: 0,
			gauge_rows: 0,

			// pattern
			pattern_gauge_stitches: 0,
			pattern_gauge_rows: 0,
			pattern_final_stitches: 0,
			pattern_final_rows: 0,
			gauge_adjust_stitches: 0,
			gauge_adjust_rows: 0,

			// goal
			method: 1, // {1: measurement, 2: stitches, 3: repeat}
			goal_stitches: 0,
			goal_rows: 0,
			goal_width: 0,
			goal_length: 0,
			goal_repeat_x: 0,
			goal_repeat_y: 0

		},

		name: "Gauge",

		initialize: function() {
			//console.log(this.escape("title"));
		},

		// validate: function(attrs) {
		// 	// title
		// 	if (attrs.title && typeof attrs.title != "string") {
		// 		return "Title must be a text value";
		// 	}
		// 	if (attrs.title && attrs.title.length < 1) {
		// 		return "Please enter a title";
		// 	}
		// },

		setGaugeValues: function(values) {
			if(typeof values == "object") {
				var gauge_stitches = (Math.round(((values.swatch_stitches)/(values.swatch_width))* 10) / 10);
				var gauge_rows = (Math.round(((values.swatch_rows)/(values.swatch_length))* 10) / 10);
				_.extend(values, {
					'gauge_stitches': gauge_stitches,
					'gauge_rows'	: gauge_rows
				});
				this.set(values);
				this.refreshGoal();
				this.refreshPattern();
			}
		},

		setPatternValues: function(values) {
			if(typeof values == "object") {
				this.set(values);
				this.refreshPattern();
			}
		},

		setGoalValues: function(values) {
			if(typeof values == "object") {
				this.set(values);
				this.refreshGoal();
			}
		},

		refreshPattern: function() {
			var pattern_gauge_stitches = this.get("pattern_gauge_stitches");
			var pattern_gauge_rows = this.get("pattern_gauge_rows");
			var pattern_final_stitches = this.get("pattern_final_stitches");
			var pattern_final_rows = this.get("pattern_final_rows");
			if(pattern_gauge_stitches != null && pattern_gauge_stitches != 0
				&& pattern_gauge_rows != null && pattern_gauge_rows != 0
				&& pattern_final_stitches != null && pattern_final_stitches != 0
				&& pattern_final_rows != null && pattern_final_rows != 0) {
				this.adjustCalculate(pattern_gauge_stitches, pattern_gauge_rows, pattern_final_stitches, pattern_final_rows);
			}
		},

		adjustCalculate: function(pattern_gauge_stitches, pattern_gauge_rows, pattern_final_stitches, pattern_final_rows) {
			var swatch_width = this.get("swatch_width");
			var gauge_stitches = this.get("gauge_stitches");
			var pattern_gauge_stitches_per = pattern_gauge_stitches/swatch_width;
			var gauge_adjust_stitches = (Math.round(((gauge_stitches)/(pattern_gauge_stitches_per))*pattern_final_stitches* 10) / 10);
			
			var swatch_length = this.get("swatch_length");
			var gauge_rows = this.get("gauge_rows");
			var pattern_gauge_rows_per = pattern_gauge_rows/swatch_length;
			var gauge_adjust_rows = (Math.round(((gauge_rows)/(pattern_gauge_rows_per))*pattern_final_rows* 10) / 10);

			this.set({
				"pattern_gauge_stitches"	: pattern_gauge_stitches,
				"pattern_gauge_rows"		: pattern_gauge_rows,
				"pattern_final_stitches"	: pattern_final_stitches,
				"pattern_final_rows"		: pattern_final_rows,
				"gauge_adjust_stitches"		: gauge_adjust_stitches,
				"gauge_adjust_rows" 		: gauge_adjust_rows
			});
			this.save();
		},

		refreshGoal: function() {
			switch(this.get("method")) {
				case 1:
					var goal_width = this.get("goal_width");
					var goal_length = this.get("goal_length");
					if(goal_width != null && goal_width != 0 && goal_length != null && goal_length != 0) {
						this.createByMeasurement(goal_width, goal_length);
					}
					break;
				case 2:
					var goal_stitches = this.get("goal_stitches");
					var goal_rows = this.get("goal_rows");
					if(goal_stitches != null && goal_stitches != 0 && goal_rows != null && goal_rows != 0) {
						this.createByStitches(goal_stitches, goal_rows);
					}
					break;
				case 3:
					var goal_repeat_x = this.get("goal_repeat_x");
					var goal_repeat_y = this.get("goal_repeat_y");
					if(goal_repeat_x != null && goal_repeat_x != 0 && goal_repeat_y != null && goal_repeat_y != 0) {
						this.createByRepeat(goal_repeat_x, goal_repeat_y);
					}
					break;
			}
		},

		createByMeasurement: function(goal_width, goal_length) {
			// calculate stitches x rows
			var gauge_stitches = this.get("gauge_stitches");
			var gauge_rows = this.get("gauge_rows");
			var goal_stitches = (Math.round((gauge_stitches*goal_width)* 10) / 10);
			var goal_rows = (Math.round((gauge_rows*goal_length)* 10) / 10);

			// calculate repeat
			var swatch_width = this.get("swatch_width");
			var swatch_length = this.get("swatch_length");
			var goal_repeat_x = (Math.round((goal_width/swatch_width)* 10) / 10);
			var goal_repeat_y = (Math.round((goal_length/swatch_length)* 10) / 10);

			this.set({
				"method"		: 1,
				"goal_width"	: goal_width,
				"goal_length"	: goal_length,
				"goal_stitches"	: goal_stitches,
				"goal_rows"		: goal_rows,
				"goal_repeat_x"	: goal_repeat_x,
				"goal_repeat_y" : goal_repeat_y
			});
			this.save();
		},

		createByStitches: function(goal_stitches, goal_rows) {
			// calculate measurement
			var gauge_stitches = this.get("gauge_stitches");
			var gauge_rows = this.get("gauge_rows");
			var goal_width = (Math.round(((goal_stitches)/(gauge_stitches))* 10) / 10);
			var goal_length = (Math.round(((goal_rows)/(gauge_rows))* 10) / 10);

			// calculate repeat
			var swatch_stitches = this.get("swatch_stitches");
			var swatch_rows = this.get("swatch_rows");
			var goal_repeat_x = (Math.round(((goal_stitches)/(swatch_stitches))* 10) / 10);
			var goal_repeat_y = (Math.round(((goal_rows)/(swatch_rows))* 10) / 10);

			this.set({
				"method"		: 2,
				"goal_width"	: goal_width,
				"goal_length"	: goal_length,
				"goal_stitches"	: goal_stitches,
				"goal_rows"		: goal_rows,
				"goal_repeat_x"	: goal_repeat_x,
				"goal_repeat_y" : goal_repeat_y
			});
			this.save();
		},

		createByRepeat: function(goal_repeat_x, goal_repeat_y) {
			// calculate measurement
			var swatch_width = this.get("swatch_width");
			var swatch_length = this.get("swatch_length");
			var goal_width = (Math.round(((swatch_width)*(goal_repeat_x))* 10) / 10);
			var goal_length = (Math.round(((swatch_length)*(goal_repeat_y))* 10) / 10);

			// calculate stitches x rows
			var swatch_stitches = this.get("swatch_stitches");
			var swatch_rows = this.get("swatch_rows");
		 	var goal_stitches = (Math.round(((swatch_stitches)*(goal_repeat_x))* 10) / 10);
			var goal_rows = (Math.round(((swatch_rows)*(goal_repeat_y))* 10) / 10);

			this.set({
				"method"		: 3,
				"goal_width"	: goal_width,
				"goal_length"	: goal_length,
				"goal_stitches"	: goal_stitches,
				"goal_rows"		: goal_rows,
				"goal_repeat_x"	: goal_repeat_x,
				"goal_repeat_y" : goal_repeat_y
			});
			this.save();
		}
		
	});

	return Gauge;

}); // define