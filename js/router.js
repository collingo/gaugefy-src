define([
	'views/gauge',
	'views/adjust',
	'views/create'
], function(GaugeView, AdjustView, CreateView) {


	function StateMachine(current) {
		// static properties
		this.name = "StateMachine";
		this.current = current;
		this.breadcrumb = new Array();

		_.bindAll(this, "orientationchange");

		// detect orientation change
		$(window).bind('orientationchange', this.orientationchange);
	}
	StateMachine.prototype = {
		log: function() {
			var prefix = this.name + " >";
			var items = Array.prototype.slice.call(arguments, 0);
			items.unshift(prefix);
			console.log.apply(console, items);
		},
		orientationchange: function(e) {
            $(this.current.el).trigger("scrollRefresh");
		},
		goTo: function(route, sibling) {
			// check if route is the current page before proceeding
			if(!(route == this.current)) {
				// check that the route is not in the breadcrumb
				if(!this.isInBreadcrumb(route)) {
					// animate forwards
					this.showChild(route);
				} else {
					// if it is, animate backwards
					this.showParent();
				}
			}
		},
		isInBreadcrumb: function(route) {
			return (this.breadcrumb.length && this.breadcrumb[this.breadcrumb.length -1] == route) ? true : false;
		},
		showChild: function(child) {
			// make current parent by pushing into breadcrumb
			if(this.current.name == "GaugeNewView") {
				// hide new page
				$(this.current.el).removeClass("current");
			} else {
				this.breadcrumb.push(this.current);
				// hide current
				$(this.current.el).addClass("stageleft");
			}
			// add new child as current
			this.current = child;
			$(this.current.el).addClass("current");
		},
		showParent: function() {
			// remove current child
			$(this.current.el).removeClass("current");
			$("#jqt > div").removeClass("prepare");
			// new current is next parent which is popped off the breadcrumb
			this.current = this.breadcrumb.pop();
			$(this.current.el).removeClass("stageleft");
		}
	}


	var Router = Backbone.Router.extend({

		routes: {
			"/:controller/:method/:id"	: "controllerMethodId",
			"/:controller/new"			: "controllerId",
			"/:controller/:id"			: "controllerId",
			"/:controller"				: "controller",
			"*actions"    				: "defaultRoute"
		},

		name: "Router",

		initialize: function(gauges, homeView, settingsView, globals) {

			// store gauges collection for reference
			this.gauges = gauges;

			// store units setting for reference
			this.globals = globals;

			// initiate all pages and store locally for reference
			this.homeView = homeView;
			this.settingsView = settingsView;
			this.gaugeView = new GaugeView(this.globals);
			this.createView = new CreateView();
			this.adjustView = new AdjustView();

			// bind to app wide unit change
			$(document).bind("refreshGlobals", this.refreshGlobals);

			// initiate new statemachine to handle app navigation
			this.statemachine = new StateMachine(this.homeView);

		},

		// route handlers
		controllerMethodId: function( controller, method, cid ) {
			// model
			var model = this.gauges.getByCid(cid);

			// setup the view to be shown
			this[method+"View"].setModel(model);

			// send request to statemachine
			this.statemachine.goTo(this[method+"View"]);
		},
		controllerId: function( controller, cid ) {
			if(typeof cid != "undefined") {
				// view gauge with cid
				var model = this.gauges.getByCid(cid);
			} else {
				// create new model and bind to newView
		  		this.gauges.add({});
		  		var model = this.gauges.at(this.gauges.length - 1);
			}
			// bind the model to the gaugeView
			this.gaugeView.setModel(model);

			// send request to statemachine
			this.statemachine.goTo(this.gaugeView);
		},
		controller: function( controller ) {
			if(controller == "") {
				this.statemachine.goTo(this.homeView);
			} else {
				this.statemachine.goTo(this[controller+"View"]);
		    }
		},
		defaultRoute: function( actions ) {
			this.log("Unknown route:", actions );
		},

		// event handlers
		refreshGlobals: function(e, globals) {
			this.globals = globals;
		},

		// utility
		log: function() {
			var prefix = this.name + " >";
			var items = Array.prototype.slice.call(arguments, 0);
			items.unshift(prefix);
			console.log.apply(console, items);
		}

	});

	return Router;

});