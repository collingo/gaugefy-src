define([
	'models/gauge'
], function(Gauge){

	var GaugefyCollection = Backbone.Collection.extend({
		name: "GenericGaugefyCollection",
		log: function() {
			var prefix = "Collections > " + this.name + " >";
			var items = Array.prototype.slice.call(arguments, 0);
			items.unshift(prefix);
			console.log.apply(console, items);
		}
	});

	var GaugeCollection = GaugefyCollection.extend({
		
		model: Gauge,

		name: "Gauges",

		localStorage: new Store("Gauges"),

		initialize: function() {
			_.bindAll(this);
		}

	});

	return GaugeCollection;

}); // define