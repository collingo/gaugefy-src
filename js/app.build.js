({
    appDir: "../",
    baseUrl: "./js",
    dir: "../../xcode/www",
    modules: [
        {
            name: "main"
        }
    ],
    paths: {
		templates: '../templates'
	}
})