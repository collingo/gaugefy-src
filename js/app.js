define([

	// // libraries
	// 'Zepto', 
	// 'Underscore', 
	// 'Backbone',

	// initial view
	'views/home/home',
	'views/settings',

	// router
	'router',

	// collections
	'collections/gauges'

], function(HomeView, SettingsView, Router, GaugeCollection) {

	// scroll fixes
	if(window.Touch) {
		//if (("standalone" in window.navigator) && window.navigator.standalone) {
			$(".toolbar").live('touchmove', function(e) {
				e.preventDefault();
			});
		//}
	}

	var app = {
		name: "Gaugefy",
		log: function() {
			var items = Array.prototype.slice.call(arguments, 0);
			items.unshift(app.name);
			console.log.apply(console, items);
		}
	};

	// setup collections
	app.gauges = new GaugeCollection();
	app.gauges.fetch();

	app.homeView = new HomeView({collection:app.gauges});

	// setup global variables
	if(localStorage.gaugefy) {
		app.globals = JSON.parse(localStorage.gaugefy);
	} else {
		app.globals = {
			unit:"imperial",
			needle_unit:"imperial"
		}
	}

	$(document).bind("setGlobals", function(e, newGlobals) {
		_.extend(app.globals, newGlobals);
		localStorage.gaugefy = JSON.stringify(app.globals);
		$(document).trigger("refreshGlobals", app.globals);
	});
	$(document).bind("getGlobals", function(e) {
		$(document).trigger('setGlobals', {});
	});

	app.settingsView = new SettingsView(app.globals);

	// Initiate the router passing in the gauges collection for reference
	app.router = new Router(app.gauges, app.homeView, app.settingsView, app.globals);
	// Start Backbone history a neccesary step for bookmarkable URL's
	Backbone.history.start();

	return app;

});